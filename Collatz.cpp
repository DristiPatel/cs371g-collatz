// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert
#include <stdint.h>

#include "Collatz.hpp"

using namespace std;

unsigned const int CACHE_SIZE = 1000000;
unsigned int cache [CACHE_SIZE]; /*Cache to store calculated values*/


// ----------------
// cycle_length
// ----------------

unsigned cycle_length (unsigned long int n) {

    //Base case for recursion
    if(n == 1) {
        return 1;
    }

    //Check if cycles for n is already cached
    if(n < CACHE_SIZE && cache[n] != 0) {
        return cache[n];
    }

    //Store current cycle length in c so it can be cached
    unsigned c = 0;
    //Call recursion
    if(n % 2 == 0) {
        c = 1 + cycle_length(n / 2);
    } else {
        c = 1 + cycle_length(n * 3 + 1);
    }

    if(n < CACHE_SIZE) {
        cache[n] = c;
    }
    return c;
}

// ----------------
// max_cycle_length
// ----------------


unsigned max_cycle_length (unsigned long int i, unsigned long int j) {
    assert(i > 0);
    assert(j > 0);

    //swap i and j so that i <= j
    if (i > j) {
        int32_t temp = i;
        i = j;
        j = temp;
    }

    unsigned v = 1;
    for(unsigned long int n = i; n <= j; ++n) {

        unsigned c = cycle_length(n);

        //save final result to cache
        if(n < CACHE_SIZE) {
            cache[n] = c;
        }
        if(c > v) {
            v = c;
        }
    }
    assert(v > 0);
    return v;
}


