// -----------
// Collatz.hpp
// -----------

#ifndef Collatz_hpp
#define Collatz_hpp



// ----------------
// cycle_length
// ----------------

/**
 * Taken in an integer and returns the cycle length for Collatz sequence.
 *
 * This implementation uses recursion and caches the cycle length for
 * every value in the path of n to speed up further calculations.
 *
 * @param n integer to find Collatz cycle_length for
 * @return positive integer
 */


unsigned cycle_length (unsigned long int n);

// ----------------
// max_cycle_length
// ----------------

/**
 * Takes in a range of integers and find the largest Collatz cycle length for any number in the range (inclusive)
 *
 * This method caches the final cycle length of each int in range resulting from cycle_length()
 *
 * @param two positive ints
 * @return positive integer
 */

unsigned max_cycle_length (unsigned long int i, unsigned long int j);

#endif // Collatz_hpp
