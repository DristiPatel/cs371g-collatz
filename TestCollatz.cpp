// ---------------
// TestCollatz.cpp
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----------------
// max_cycle_length
// ----------------

TEST(CollatzFixture, max_cycle_length_0) {
    ASSERT_EQ(max_cycle_length(1, 10), 20);
}

TEST(CollatzFixture, max_cycle_length_1) {
    ASSERT_EQ(max_cycle_length(100, 200), 125);
}

TEST(CollatzFixture, max_cycle_length_2) {
    ASSERT_EQ(max_cycle_length(201, 210), 89);
}

TEST(CollatzFixture, max_cycle_length_3) {
    ASSERT_EQ(max_cycle_length(900, 1000), 174);
}

TEST(CollatzFixture, max_cycle_length_4) {
    ASSERT_EQ(max_cycle_length(267654, 274372), 407);
}

TEST(CollatzFixture, max_cycle_length_5) {
    ASSERT_EQ(max_cycle_length(1, 1), 1);
}

TEST(CollatzFixture, max_cycle_length_6) {
    ASSERT_EQ(max_cycle_length(27, 27), 112);
}

TEST(CollatzFixture, max_cycle_length_7) {
    ASSERT_EQ(max_cycle_length(10, 1), 20);
}

TEST(CollatzFixture, max_cycle_length_8) {
    ASSERT_EQ(max_cycle_length(50, 51), 25);
}

TEST(CollatzFixture, max_cycle_length_9) {
    ASSERT_EQ(max_cycle_length(999, 1000), 112);
}

TEST(CollatzFixture, max_cycle_length_10) {
    ASSERT_EQ(max_cycle_length(65536, 65536), 17);
}

TEST(CollatzFixture, max_cycle_length_11) {
    ASSERT_EQ(max_cycle_length(10000000, 10000000), 146);
}

TEST(CollatzFixture, max_cycle_length_12) {
    ASSERT_EQ(max_cycle_length(10000000, 10000001), 146);
}

TEST(CollatzFixture, max_cycle_length_13) {
    ASSERT_EQ(max_cycle_length(255, 255), 48);
}

TEST(CollatzFixture, max_cycle_length_14) {
    ASSERT_EQ(max_cycle_length(639, 639), 132);
}

TEST(CollatzFixture, max_cycle_length_15) {
    ASSERT_EQ(max_cycle_length(703, 703), 171);
}

TEST(CollatzFixture, max_cycle_length_16) {
    ASSERT_EQ(max_cycle_length(10000, 10000), 30);
}

TEST(CollatzFixture, max_cycle_length_17) {
    ASSERT_EQ(max_cycle_length(98, 102), 26);
}

TEST(CollatzFixture, max_cycle_length_18) {
    ASSERT_EQ(max_cycle_length(271114753, 271114879), 280);
}

TEST(CollatzFixture, max_cycle_length_19) {
    ASSERT_EQ(max_cycle_length(63728127, 63728127), 950);
}

TEST(CollatzFixture, max_cycle_length_20) {
    ASSERT_EQ(max_cycle_length(9780657630, 9780657630), 1133);
}
